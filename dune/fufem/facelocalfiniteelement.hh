// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef FACE_LOCAL_FINITE_ELEMENT_HH
#define FACE_LOCAL_FINITE_ELEMENT_HH

#include <vector>

#include <dune/localfunctions/common/localfiniteelementtraits.hh>
#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>

template <class Traits>
struct FaceTraits
{
    //! \brief Export type for domain field
    typedef typename Traits::DomainFieldType DomainFieldType;

    //! \brief Enum for domain dimension
    enum {
      //! \brief dimension of the domain
      dimDomain = Traits::dimDomain-1
    };

    //! \brief domain type, this might not be correct for non-standard bases!
    typedef Dune::FieldVector<DomainFieldType,dimDomain> DomainType;

    //! \brief Export type for range field
    typedef typename Traits::RangeFieldType RangeFieldType;

    //! \brief Enum for range dimension
    enum {
        //! \brief dimension of the range
        dimRange = Traits::dimRange
    };

    //! \brief range type
    typedef typename Traits::RangeType RangeType;

    //! \brief type of the jacobian
    typedef typename Traits::JacobianType JacobianType;

    //! \brief Enum for differentiability order
    enum {
        //! \brief number of partial derivatives supported
        diffOrder=  Traits::diffOrder
    };
};




template <class LocalBasis,class LocalGeometry>
class FaceLocalBasis
{
public:
    typedef FaceTraits<typename LocalBasis::Traits> Traits;

    FaceLocalBasis(const LocalBasis& localBasis, const LocalGeometry& geometryInInside) :
        localBasis_(localBasis),
        geometryInInside_(geometryInInside)
    {}

    //! \brief number of shape functions
    unsigned int size () const
    {
        return localBasis_.size() ;
    }

    //! \brief Evaluate all shape functions at local coordinates of the intersection
    inline void evaluateFunction (const typename Traits::DomainType& in,
                                  std::vector<typename Traits::RangeType>& out) const
    {
        out.resize(size());

        // transform the coordinates to the inside of the intersection
        typename LocalBasis::Traits::DomainType inInside = geometryInInside_.global(in);

        // then evaluate the basis of that element
        localBasis_.evaluateFunction(inInside, out);
    }

    //! \brief Evaluate Jacobian of all shape functions at local coordinates of the intersection
    inline void evaluateJacobian (
        const typename Traits::DomainType& in,                  // position
        std::vector<typename Traits::JacobianType>& out) const  // return value
    {
        out.resize(size());

        // transform the coordinates to the inside of the intersection
        typename LocalBasis::Traits::DomainType inInside = geometryInInside_.global(in);

        // then evaluate the basis of that element
        localBasis_.evaluateJacobian(inInside, out);
    }

    //! \brief Polynomial order of the shape functions
    unsigned int order () const
    {
        return localBasis_.order();
    }

private:
    const LocalBasis& localBasis_;
    const LocalGeometry& geometryInInside_;
};



/** \brief A wrapper class that wraps a LocalFiniteElement of an entity to a basis that lives on an intersection of that entity, specified by the LocalGeometry */
template <class LocalFiniteElement, class LocalGeometry>
class FaceLocalFiniteElement
{
public:

    typedef FaceLocalBasis<typename LocalFiniteElement::Traits::LocalBasisType,LocalGeometry> LocalBasis;

    typedef Dune::LocalFiniteElementTraits< LocalBasis, typename LocalFiniteElement::Traits::LocalCoefficientsType,
                                      typename LocalFiniteElement::Traits::LocalInterPolationType> Traits;

    FaceLocalFiniteElement(const LocalFiniteElement& lFE, const LocalGeometry& geometryInInside) :
        lFE_(lFE),
        geometryInInside_(geometryInInside),
        localBasis_(lFE_.localBasis(),geometryInInside)
    {}

    //! \brief Get the local basis
    const typename Traits::LocalBasisType& localBasis() const
    {
        return localBasis_;
    }

    //! \brief Get the local coefficients
    const typename Traits::LocalCoefficientsType& localCoefficients () const
    {
        return lFE_.localCoefficients();
    }

    //! \brief Get the local interpolation
    const typename Traits::LocalInterpolationType& localInterpolation () const
    {
        return lFE_.localInterpolation();
    }

    //! \brief Get the geometry type the basis is for
    // What type is it the for the intersection or the inside type?
    Dune::GeometryType type () const
    {
        return geometryInInside_.type();
    }

private:
    const LocalFiniteElement& lFE_;
    const LocalGeometry& geometryInInside_;
    const LocalBasis& localBasis_;

};



#endif

