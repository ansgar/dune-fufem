#pragma once

#warning This file is deprecated!  Please use Dune::Fufem::MassAssembler from the file massassembler.hh!

#include <dune/fufem/assemblers/localassemblers/massassembler.hh>

namespace Dune::Fufem
{
  using DuneFunctionsLocalMassAssembler = Dune::Fufem::MassAssembler;
} // namespace Dune::Fufem
