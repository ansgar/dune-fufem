// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_HESSIAN_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_HESSIAN_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

/** \brief Local finite element assembler that computes the exact hessian of an energy functional
 *
 *      using the automatic differentiation library ADOL-C. Needs a local energy that implements the
 *      Adolc::LocalEnergy interface.
 *
 * */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class AdolcHessianAssembler :
  public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >
{
  private:
    typedef LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T> Base;

    typedef typename T::field_type field_type;
    enum {blocksize=T::rows};

    static_assert(T::rows == T::cols, "Rows and Columns of the block type have to be equal");

    typedef Adolc::LocalEnergy<GridType, AnsatzLocalFE, blocksize> LocalEnergy;
    typedef std::vector<Dune::FieldVector<field_type,blocksize> > CoefficientVectorType;

    typedef VirtualGridFunction<GridType, Dune::FieldVector<field_type,blocksize> > GridFunction;

    typedef typename TrialLocalFE::Traits::LocalBasisType::Traits FunctionTraits;
    typedef LocalFunctionComponentWrapper<GridFunction, GridType, FunctionTraits> LocalWrapper;

  public:
    typedef typename Base::Element Element;
    typedef typename Element::Geometry Geometry;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

    /** \brief Default constructor */
    AdolcHessianAssembler() {}

    /**
     * \brief Construct HessianAssembler
     *
     * Creates a local assembler that computes the exact hessian of a
     * given energy functional using the automatic differentiation library Adol-C
     *
     * \param energy The energy functional
     * \param configuration The point at which the hessian is computed
     *
     */
    AdolcHessianAssembler(const LocalEnergy& energy, const GridFunction& configuration, bool vectorMode=true) :
        vectorMode_(vectorMode),
        energy_(&energy),
        configuration_(&configuration)
    {}

    /**
     * \brief Construct HessianAssembler
     *
     * Creates a local assembler that computes the exact hessian of a
     * given energy functional using the automatic differentiation library Adol-C
     *
     * \param energy The energy functional
     */
    AdolcHessianAssembler(const LocalEnergy& energy, bool vectorMode=true) :
        vectorMode_(vectorMode),
        energy_(&energy)
    {}

    void indices(const Element& element,
            BoolMatrix& isNonZero,
            const TrialLocalFE& tFE,
            const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    //! Assemble the local hessian
    void assemble(const Element& element, LocalMatrix& localMatrix,
                  const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {

        // interpolate by local finite element to get the local coefficients
        // assume that the configuration grid function corresponds to the ansatz space
        CoefficientVectorType localCoeff(aFE.localBasis().size());
        LocalWrapper fiLocal(*configuration_,element,0);

        std::vector<typename LocalWrapper::RangeType> interpolationValues;
        for (int i=0; i<blocksize; i++) {
            fiLocal.setIndex(i);

            aFE.localInterpolation().interpolate(fiLocal,interpolationValues);
            for (size_t j=0; j<aFE.localBasis().size(); j++)
                localCoeff[j][i] = interpolationValues[j];
        }

        // call assemble method
        // this assembler computes the hessian of an energy functional
        // so we don't need the test finite element space
        assembleMatrix(element, localCoeff, localMatrix, aFE);
    }

    /** \brief Set configuration at which point the hessian is evaluated. */
    void setConfiguration(const GridFunction& configuration)
    {
        configuration_ = &configuration;
    }

    /** \brief Set configuration at which point the hessian is evaluated. */
    void setConfiguration(std::shared_ptr<GridFunction> configuration)
    {
        configuration_ = configuration.get();
    }

  private:
    //! Assemble the local hessian
    void assembleMatrix(const Element& element, const CoefficientVectorType& localCoeff,
            LocalMatrix& localMatrix, const AnsatzLocalFE& aFE) const
    {
        localMatrix = 0.0;

        // Tape energy computation.
        // We may not have to do this every time, but it's comparatively cheap.
        Adolc::tapeEnergy(element, aFE, localCoeff,energy_);

        ///////////////////////////
        //  Compute the hessian
        ///////////////////////////

        // Copy data from Dune data structures to plain-C ones
        size_t nDofs = localCoeff.size();
        size_t nDoubles = nDofs*blocksize;

        std::vector<field_type> xp(nDoubles);
        int idx=0;
        for (size_t i=0; i<nDofs; i++)
            for (size_t j=0; j<blocksize; j++)
                xp[idx++] = localCoeff[i][j];

        field_type* rawHessian[nDoubles];
        for(size_t i=0; i<nDoubles; i++)
            rawHessian[i] = new field_type[i+1];

        if (vectorMode_)
            hessian2(1,nDoubles,xp.data(),rawHessian);
        else
            hessian(1,nDoubles,xp.data(),rawHessian);

        // Copy Hessian into Dune data type
        for(size_t i=0; i<nDoubles; i++)
            for (size_t j=0; j<nDoubles; j++)
            {
                field_type value = (i>=j) ? rawHessian[i][j] : rawHessian[j][i];
                localMatrix[j/blocksize][i/blocksize][j%blocksize][i%blocksize] = value;
            }

        for(size_t i=0; i<nDoubles; i++)
            delete[] rawHessian[i];

        return;
    }

    //! Use vector mode of Adol-C?
    bool vectorMode_;

    //! The energy functional that we compute the hessian of
    const LocalEnergy* energy_;

    //! Grid function representing the configuration at which the hessian is evaluated
    const GridFunction* configuration_;
};

#endif

