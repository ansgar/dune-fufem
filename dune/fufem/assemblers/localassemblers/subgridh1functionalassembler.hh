// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUBGRID_H1_FUNCTIONAL_ASSEMBLER_HH
#define SUBGRID_H1_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/function.hh>
#include <dune/grid/common/rangegenerators.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>


#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/h1functionalassembler.hh>

/**  \brief Local assembler for finite element H^1-functionals on the Subgrid, given by Hostgrid-functions
  *
  *  This is needed, e.g. when assembling the right hand side of the spatial problem of a time-discretized time dependent problem with spatial adaptivity.
  *  The solution of the old time step lives on the hostgrid while the rhs is assembled on the NEW subgrid.
  */
template <class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,1> >
class SubgridH1FunctionalAssembler :
    public H1FunctionalAssembler<GridType, TrialLocalFE, T>

{
    private:
        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::HostGridType HostGrid;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;
        typedef typename DerivativeTypefier<GlobalCoordinate,T>::DerivativeType FRangeType;
        typedef VirtualGridFunction<GridType, FRangeType> GridFunction;
        typedef VirtualGridFunction<HostGrid, FRangeType> HostGridFunction;

        typedef BasisGridFunctionInfo<HostGrid> HostBasisGridFunctionInfo;

    public:
        typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::LocalVector LocalVector;

        typedef typename GridType::HostGridType::template Codim<0>::Entity::Geometry HostGeometry;

        typedef typename Dune::VirtualFunction<GlobalCoordinate, FRangeType> Function;

        /** \brief constructor
          *
          * \param f the (hostgrid) function representing the functional (if assembling e.g. \f$(\nabla u,\nabla v)\f$ you need to provide \f$f=\nabla u\f$)
          * \param grid the subgrid (!) type
          * \param order the quadrature order (DEFAULT 2)
          */
        SubgridH1FunctionalAssembler(const Function& f, const GridType& grid, int order=2) :
            H1FunctionalAssembler<GridType,TrialLocalFE,T>(f, order),
            f_(f),
            grid_(grid),
            order_(order),
            fAsHostGridFunction_(dynamic_cast<const HostGridFunction*>(&f)),
            hostBasisGridFunctionInfo_(dynamic_cast<const HostBasisGridFunctionInfo*>(&f))
        {}

        /** \copydoc H1FunctionalAssembler::assemble
         */
        void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
        {
            if (not(fAsHostGridFunction_))
            {
                H1FunctionalAssembler<GridType,TrialLocalFE,T>::assemble(element, localVector, tFE);
                return;
            }

            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType LocalBasisJacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            const auto hostelement = grid_.template getHostEntity<0>(element);

            bool trialSpaceIsRefined = IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE);

            // store gradients of shape functions and base functions
            std::vector<LocalBasisJacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<GlobalCoordinate> gradients(tFE.localBasis().size());

            if (hostelement.isLeaf())
            {
                bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(hostelement);

                // get quadrature rule
                const auto& quad = QuadratureRuleCache<double, dim>::rule(element.type(), order_, (trialSpaceIsRefined or hostGFIsRefined) );

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const LocalCoordinate& quadPos = quad[pt].position();

                    // get transposed inverse of Jacobian of transformation
                    const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // get gradients of shape functions
                    tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                    // transform gradients
                    for (size_t i=0; i<gradients.size(); ++i)
                        invJacobian.mv(referenceGradients[i][0], gradients[i]);

                    // compute values of function
                    FRangeType f_pos;
                    fAsHostGridFunction_->evaluateLocal(hostelement, quadPos, f_pos);

                    // and vector entries
                    for (size_t i=0; i<gradients.size(); ++i)
                    {
                        T dummy;
                        f_pos.mv(gradients[i],dummy);
                        localVector[i].axpy(quad[pt].weight()*integrationElement, dummy);
                    }
                }
            }
            else // corresponding hostgrid element is not in hostgrid leaf
            {
                for (const auto& descElement : descendantElements(hostelement, grid_.getHostGrid().maxLevel()))
                {
                    if (descElement.isLeaf())
                    {
                        const HostGeometry hostGeometry = descElement.geometry();

                        bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(descElement);

                        // get quadrature rule
                        const auto& quad = QuadratureRuleCache<double, dim>::rule(descElement.type(), order_, hostGFIsRefined);

                        // loop over quadrature points
                        for (size_t pt=0; pt < quad.size(); ++pt)
                        {
                            // get quadrature point
                            const LocalCoordinate& quadPos = quad[pt].position();
                            const LocalCoordinate quadPosInSubgridElement = geometry.local(hostGeometry.global(quadPos)) ;

                            // get integration factor
                            const double integrationElement = hostGeometry.integrationElement(quadPos);

                            // get transposed inverse of Jacobian of transformation
                            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                            // get gradients of shape functions
                            tFE.localBasis().evaluateJacobian(quadPosInSubgridElement, referenceGradients);

                            // transform gradients
                            for (size_t i=0; i<gradients.size(); ++i)
                                invJacobian.mv(referenceGradients[i][0], gradients[i]);

                            // compute values of function
                            FRangeType f_pos;
                            fAsHostGridFunction_->evaluateLocal(descElement, quadPos, f_pos);

                            // and vector entries
                            for (size_t i=0; i<gradients.size(); ++i)
                            {
                                T dummy;
                                f_pos.mv(gradients[i],dummy);
                                localVector[i].axpy(quad[pt].weight()*integrationElement, dummy);
                            }
                        }
                    }
                }
            }

            return;
        }

    private:
        const Function& f_;
        const GridType& grid_;
        const int order_;
        const HostGridFunction* fAsHostGridFunction_;
        const HostBasisGridFunctionInfo* hostBasisGridFunctionInfo_;
};

#endif

