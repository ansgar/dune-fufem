// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH

#include <type_traits>

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"


namespace Dune {
namespace Fufem {


//! Generic global assembler for operators on a gridview
template <class TrialBasis, class AnsatzBasis>
class DuneFunctionsOperatorAssembler
{
  using GridView = typename TrialBasis::GridView;

  // Zero-initialize local entries contained in the tree.
  // In case of a subspace basis, this does not touch
  // entries not contained in the subspace.
  template<class TrialLocalView, class AnsatzLocalView, class LocalMatrix>
  static void zeroInitializeLocal(const TrialLocalView& trialLocalView, const AnsatzLocalView& ansatzLocalView, LocalMatrix& localMatrix)
  {
    for (size_t i=0; i<trialLocalView.tree().size(); ++i)
    {
      auto localRow = trialLocalView.tree().localIndex(i);
      for (size_t j=0; j<ansatzLocalView.tree().size(); ++j)
      {
        auto localCol = ansatzLocalView.tree().localIndex(j);
        localMatrix[localRow][localCol] = 0;
      }
    }
  }

  // Distribute local matrix pattern to global pattern builder
  // In case of a subspace basis, this does not touch
  // entries not contained in the subspace.
  template<class TrialLocalView, class AnsatzLocalView, class PatternBuilder>
  static void distributeLocalPattern(const TrialLocalView& trialLocalView, const AnsatzLocalView& ansatzLocalView, PatternBuilder& patternBuilder)
  {
    for (size_t i=0; i<trialLocalView.tree().size(); ++i)
    {
      auto localRow = trialLocalView.tree().localIndex(i);
      auto row = trialLocalView.index(localRow);
      for (size_t j=0; j<ansatzLocalView.tree().size(); ++j)
      {
        auto localCol = ansatzLocalView.tree().localIndex(j);
        auto col = ansatzLocalView.index(localCol);
        patternBuilder.insertEntry(row,col);
      }
    }
  }

  // Distribute local matrix entries to global matrix
  // In case of a subspace basis, this does not touch
  // entries not contained in the subspace.
  template<class TrialLocalView, class AnsatzLocalView, class LocalMatrix, class MatrixBackend>
  static void distributeLocalEntries(const TrialLocalView& trialLocalView, const AnsatzLocalView& ansatzLocalView, const LocalMatrix& localMatrix, MatrixBackend& matrixBackend)
  {
    for (size_t i=0; i<trialLocalView.tree().size(); ++i)
    {
      auto localRow = trialLocalView.tree().localIndex(i);
      auto row = trialLocalView.index(localRow);
      for (size_t j=0; j<ansatzLocalView.tree().size(); ++j)
      {
        auto localCol = ansatzLocalView.tree().localIndex(j);
        auto col = ansatzLocalView.index(localCol);
        matrixBackend(row,col) += localMatrix[localRow][localCol];
      }
    }
  }

public:
  //! create assembler for grid
  DuneFunctionsOperatorAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis) :
      trialBasis_(tBasis),
      ansatzBasis_(aBasis)
  {}



  template <class PatternBuilder>
  void assembleBulkPattern(PatternBuilder& patternBuilder) const
  {
    patternBuilder.resize(trialBasis_, ansatzBasis_);

    // create two localViews but use only one if bases are the same
    auto ansatzLocalView = ansatzBasis_.localView();
    auto separateTrialLocalView = trialBasis_.localView();
    auto& trialLocalView = selectTrialLocalView(ansatzLocalView, separateTrialLocalView);

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      // bind the localViews to the element
      bind(ansatzLocalView, trialLocalView, element);

      distributeLocalPattern(trialLocalView, ansatzLocalView, patternBuilder);
    }
  }



  template <class PatternBuilder>
  void assembleSkeletonPattern(PatternBuilder& patternBuilder) const
  {
    patternBuilder.resize(trialBasis_, ansatzBasis_);

    auto insideTrialLocalView       = trialBasis_.localView();

    auto insideAnsatzLocalView      = ansatzBasis_.localView();

    auto outsideAnsatzLocalView     = ansatzBasis_.localView();

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      insideTrialLocalView.bind(element);

      insideAnsatzLocalView.bind(element);

      /* Coupling on the same element */
      distributeLocalPattern(insideTrialLocalView, insideAnsatzLocalView, patternBuilder);

      for (const auto& is : intersections(trialBasis_.gridView(), element))
      {
        /* Elements on the boundary have been treated above, iterate along the inner edges */
        if (is.neighbor())
        {
          // Current outside element
          const auto& outsideElement = is.outside();

          // Bind the outer parts to the outer element
          outsideAnsatzLocalView.bind(outsideElement);

          // We assume that all basis functions of the inner element couple with all basis functions from the outer one
          distributeLocalPattern(insideTrialLocalView, outsideAnsatzLocalView, patternBuilder);
        }
      }
    }
  }



  template <class MatrixBackend, class LocalAssembler>
  void assembleBulkEntries(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler) const
  {
    // create two localViews but use only one if bases are the same
    auto ansatzLocalView = ansatzBasis_.localView();
    auto separateTrialLocalView = trialBasis_.localView();
    auto& trialLocalView = selectTrialLocalView(ansatzLocalView, separateTrialLocalView);

    using Field = std::decay_t<decltype(matrixBackend(trialLocalView.index(0), ansatzLocalView.index(0)))>;
    using LocalMatrix = Dune::Matrix<Dune::FieldMatrix<Field,1,1>>;

    auto localMatrix = LocalMatrix();

    localMatrix.setSize(trialLocalView.maxSize(), ansatzLocalView.maxSize());

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      // bind the localViews to the element
      bind(ansatzLocalView, trialLocalView, element);

      zeroInitializeLocal(trialLocalView, ansatzLocalView, localMatrix);

      localAssembler(element, localMatrix, trialLocalView, ansatzLocalView);

      // Add element stiffness matrix onto the global stiffness matrix
      distributeLocalEntries(trialLocalView, ansatzLocalView, localMatrix, matrixBackend);
    }
  }


  /* This variant of the IntersectionOperatorAssembler that works
   * with dune-functions bases.
   *
   * Currently not supported are constrained bases and lumping.
   *
   * Note that this was written (and tested) having discontinuous Galerkin bases in mind.
   * There may be cases where things do not work as expected for standard (continuous) Galerkin spaces.
   */
  template <class MatrixBackend, class LocalAssembler, class LocalBoundaryAssembler>
  void assembleSkeletonEntries(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler, LocalBoundaryAssembler&& localBoundaryAssembler) const
  {
    Dune::MultipleCodimMultipleGeomTypeMapper<GridView> faceMapper(trialBasis_.gridView(), mcmgElementLayout());

    auto insideTrialLocalView       = trialBasis_.localView();

    auto insideAnsatzLocalView      = ansatzBasis_.localView();

    auto outsideTrialLocalView      = trialBasis_.localView();

    auto outsideAnsatzLocalView     = ansatzBasis_.localView();

    using Field = std::decay_t<decltype(matrixBackend(insideTrialLocalView.index(0), insideAnsatzLocalView.index(0)))>;
    using LocalMatrix = Dune::Matrix<Dune::FieldMatrix<Field,1,1>>;
    using MatrixContainer = Dune::Matrix<LocalMatrix>;
    auto matrixContrainer = MatrixContainer(2,2);

    matrixContrainer[0][0].setSize(insideTrialLocalView.maxSize(),  insideAnsatzLocalView.maxSize());
    matrixContrainer[0][1].setSize(insideTrialLocalView.maxSize(),  outsideAnsatzLocalView.maxSize());
    matrixContrainer[1][0].setSize(outsideTrialLocalView.maxSize(), insideAnsatzLocalView.maxSize());
    matrixContrainer[1][1].setSize(outsideTrialLocalView.maxSize(), outsideAnsatzLocalView.maxSize());

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      insideTrialLocalView.bind(element);

      insideAnsatzLocalView.bind(element);

      for (const auto& is : intersections(trialBasis_.gridView(), element))
      {

        /* Assemble (depending on whether we have a boundary edge or not) */
        if (is.boundary())
        {
          auto& localMatrix = matrixContrainer[0][0];

          zeroInitializeLocal(insideTrialLocalView, insideAnsatzLocalView, localMatrix);

          localBoundaryAssembler(is, localMatrix, insideTrialLocalView, insideAnsatzLocalView);

          distributeLocalEntries(insideTrialLocalView, insideAnsatzLocalView, localMatrix, matrixBackend);

        }
        else if (is.neighbor())
        {
          /* Only handle every intersection once; we choose the case where the inner element has the lower index
           *
           * This should also work with grids where elements can have different geometries. TODO: Actually test this somewhere!*/
          const auto& outsideElement = is.outside();
          if (faceMapper.index(element) > faceMapper.index(outsideElement))
            continue;


          // Bind the outer parts to the outer element
          outsideTrialLocalView.bind(outsideElement);

          outsideAnsatzLocalView.bind(outsideElement);

          zeroInitializeLocal(insideTrialLocalView,  insideAnsatzLocalView,  matrixContrainer[0][0]);
          zeroInitializeLocal(insideTrialLocalView,  outsideAnsatzLocalView, matrixContrainer[0][1]);
          zeroInitializeLocal(outsideTrialLocalView, insideAnsatzLocalView,  matrixContrainer[1][0]);
          zeroInitializeLocal(outsideTrialLocalView, outsideAnsatzLocalView, matrixContrainer[1][1]);

          localAssembler(is, matrixContrainer, insideTrialLocalView, insideAnsatzLocalView, outsideTrialLocalView, outsideAnsatzLocalView);

          distributeLocalEntries(insideTrialLocalView,  insideAnsatzLocalView,  matrixContrainer[0][0], matrixBackend);
          distributeLocalEntries(insideTrialLocalView,  outsideAnsatzLocalView, matrixContrainer[0][1], matrixBackend);
          distributeLocalEntries(outsideTrialLocalView, insideAnsatzLocalView,  matrixContrainer[1][0], matrixBackend);
          distributeLocalEntries(outsideTrialLocalView, outsideAnsatzLocalView, matrixContrainer[1][1], matrixBackend);
        }
      }
    }
  }


  template <class MatrixBackend, class LocalAssembler>
  void assembleBulk(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler) const
  {
    auto patternBuilder = matrixBackend.patternBuilder();
    assembleBulkPattern(patternBuilder);
    patternBuilder.setupMatrix();
    matrixBackend.assign(0);
    assembleBulkEntries(matrixBackend, std::forward<LocalAssembler>(localAssembler));
  }

private:

//! helper function to select a trialLocalView (possibly a reference to ansatzLocalView if bases are the same)
template<class AnsatzLocalView, class TrialLocalView>
TrialLocalView& selectTrialLocalView(AnsatzLocalView& ansatzLocalView, TrialLocalView& trialLocalView) const
{
   if constexpr (std::is_same<TrialBasis,AnsatzBasis>::value)
     if (&trialBasis_ == &ansatzBasis_)
       return ansatzLocalView;
   return trialLocalView;
}

//! small helper that checks whether two localViews are the same and binds one or both to an element
template<class AnsatzLocalView, class TrialLocalView, class E>
void bind(AnsatzLocalView& ansatzLocalView, TrialLocalView& trialLocalView, const E& e) const
{
  ansatzLocalView.bind(e);
  if constexpr (std::is_same<TrialBasis,AnsatzBasis>::value)
    if (&trialLocalView == &ansatzLocalView)
      return;
  // localViews differ: bind trialLocalView too
  trialLocalView.bind(e);
}

protected:
  const TrialBasis& trialBasis_;
  const AnsatzBasis& ansatzBasis_;
};


/**
 * \brief Create DuneFunctionsOperatorAssembler
 */
template <class TrialBasis, class AnsatzBasis>
auto duneFunctionsOperatorAssembler(const TrialBasis& trialBasis, const AnsatzBasis& ansatzBasis)
{
  return DuneFunctionsOperatorAssembler<TrialBasis, AnsatzBasis>(trialBasis, ansatzBasis);
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH

