#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/boundarypatch.hh>

#include "common.hh"



struct BoundaryPatchTestSuite
{
    template<class GridView>
    static bool checkBoundaryPatch(const GridView& gridView)
    {
        bool passed = true;

        typedef BoundaryPatch<GridView> BP;
        typedef typename BP::iterator BoundaryPatchIterator;

        BP boundary(gridView, true);

        BoundaryPatchIterator it = boundary.begin();
        BoundaryPatchIterator end = boundary.end();
        for (; it!=end; ++it)
            passed = passed and boundary.contains(it->inside(), it->indexInInside());

        // Test the assignment operator for iterators
        BoundaryPatchIterator foo = boundary.begin();
        {
            BoundaryPatchIterator bar = boundary.begin();
            bar = boundary.end();
        }
        {
            BoundaryPatchIterator bar = boundary.begin();
            bar = boundary.begin();
        }
        {
            BoundaryPatchIterator bar = boundary.begin();
            bar = foo;
        }

        // Test the copy constructor for iterators
        {
            BoundaryPatchIterator bar(foo);
        }

        // Test copy construction
        BP fooBoundary = boundary;
        [[maybe_unused]] int c;
        c = fooBoundary.gridView().indexSet().size(0);   // make the copy do something useful

        // Test assignment
        BP barBoundary;
        barBoundary = boundary;
        c = barBoundary.gridView().indexSet().size(0);   // make the copy do something useful

        return passed;
    }

    template<class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        passed = passed and checkBoundaryPatch<typename GridType::LeafGridView>(grid.leafGridView());
        for(int i=0; i<=grid.maxLevel(); ++i)
            passed = passed and checkBoundaryPatch<typename GridType::LevelGridView>(grid.levelGridView(i));

        // Test leaf copy construction
        BoundaryPatch<typename GridType::LeafGridView> leafBoundary(grid.leafGridView(), true);
        BoundaryPatch<typename GridType::LeafGridView> foo = leafBoundary;
        [[maybe_unused]] int c;
        c = foo.gridView().indexSet().size(0);   // make the copy do something useful




        return passed;
    }
};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    BoundaryPatchTestSuite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;
}
