#ifndef DUNE_FUFEM_UTILITIES_ADOLC_NAMESPACE_INJECTIONS_HH
#define DUNE_FUFEM_UTILITIES_ADOLC_NAMESPACE_INJECTIONS_HH

#include <limits>
#include <type_traits>

#include <dune/common/promotiontraits.hh>
#include <dune/common/typetraits.hh>

#ifdef HAVE_ADOLC
#include <adolc/adouble.h>

adouble sqrt_hack(adouble a) {
  return sqrt(a);
}

adouble abs_hack(adouble a) {
  return fabs(a);
}

adouble log_hack(adouble a) {
  return log(a);
}

adouble pow_hack(const adouble& a, const adouble& b) {
    return pow(a,b);
}

adouble pow_hack(const adouble& a, double b) {
    return pow(a,b);
}

adouble sin_hack(adouble a) {
  return sin(a);
}

adouble cos_hack(adouble a) {
  return cos(a);
}

adouble acos_hack(adouble a) {
  return acos(a);
}

namespace std
{
   adouble min(adouble a, adouble b) {
     // Do not use 'min', because ADOL-C lacks support for that in vector-mode
     return  (a + b - abs_hack(a - b))*0.5;
   }

   adouble max(adouble a, adouble b) {
     // Do not use 'max', because ADOL-C lacks support for that in vector-mode
     return - min(-a,-b);
   }

   adouble sqrt(adouble a) {
     return sqrt_hack(a);
   }

   adouble abs(adouble a) {
     return abs_hack(a);
   }

   adouble fabs(adouble a) {
     return abs_hack(a);
   }

   adouble log(adouble a) {
     return log_hack(a);
   }

   adouble pow(const adouble& a, const adouble& b) {
     return pow_hack(a,b);
   }

   adouble pow(const adouble& a, double b) {
     return pow_hack(a,b);
   }

   adouble sin(adouble a) {
     return sin_hack(a);
   }

   adouble cos(adouble a) {
     return cos_hack(a);
   }

   adouble acos(adouble a) {
     return acos_hack(a);
   }

   bool isnan(adouble a) {
     return std::isnan(a.value());
   }

   bool isinf(adouble a) {
     return std::isinf(a.value());
   }

   bool isfinite(adouble a) {
     return std::isfinite(a.value());
   }

   bool isnormal(adouble a) {
     return std::isnormal(a.value());
   }

   /** \brief Specialization of the numeric_limits class for adouble */
   template <>
   struct numeric_limits<adouble> {

      static adouble max() {
        return numeric_limits<double>::max();
      }

      static adouble epsilon() {
        return numeric_limits<double>::epsilon();
      }
      static adouble quiet_NaN() {
        return numeric_limits<double>::quiet_NaN();
      }

      static constexpr bool is_integer = false;

   };
}

namespace Dune
{
  template<>
  struct IsNumber<adouble>
    : std::true_type
  {};

  /* Specializations of the PromotionTraits class from dune-common for the adouble type.
   * This is needed, e.g., to be able to use the FieldVector implementation of dot products
   * between a FieldVector<double> and a FieldVector<adouble>.  The standard C++ return type
   * deduction mechanism doesn't help here, because the product of a double and an adouble
   * is actually a proxy class called 'adub', which is not to be used outside of the ADOL-C
   * library.
   */
  template <>
  struct PromotionTraits<double,adouble>
  {
    typedef adouble PromotedType;
  };

  template <>
  struct PromotionTraits<adouble,double>
  {
    typedef adouble PromotedType;
  };
}


#endif

#endif
